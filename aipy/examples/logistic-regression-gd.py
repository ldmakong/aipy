from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt

# Import helper functions
from aipy.utils import normalize, train_test_split, get_accuracy
from aipy.utils import Plot
from aipy.supervised import LogisticRegression

def main():
    #Load dataset
    data = datasets.load_iris()
    X = normalize(data.data[data.target != 0])
    y = data.target[data.target != 0]
    y[y == 1] = 0
    y[y == 2] = 1

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, seed=1)

    opt = 'gd'
    
    if opt == 'gd':

       clf = LogisticRegression(max_iter = 4000, learning_rate=1., optimizer=opt, batch_size = 0, shuffle=False, l1_beta=0.0, l2_beta=0.0)
       clf.fit(X_train, y_train)
       y_pred = clf.predict(X_test)
       acc = get_accuracy(y_test, y_pred)

       # Reduce dimension to two using PCA and plot the results
       Plot().plot_in_2d(X_test, y_pred, title="Logistic Regression - Gradient Descent", accuracy=acc)

       # Plot loss function value with respect to the number of iterations
       plt.grid(linestyle='--' )
       plt.plot(clf.losses)
       plt.suptitle(' Gradient Descent Optimizer', size = 18)
       plt.xlabel('Number of iterations', size=15)
       plt.ylabel('Binary Cross-Entropy Loss', size=15)
       plt.show()

    if opt == 'sgd': 

       clf = LogisticRegression(max_iter = 4000, learning_rate=1., optimizer=opt, batch_size = 32, shuffle=False, l1_beta=0.0, l2_beta=0.0)
       clf.fit(X_train, y_train)
       y_pred = clf.predict(X_test)
       acc = get_accuracy(y_test, y_pred)

       # Reduce dimension to two using PCA and plot the results
       Plot().plot_in_2d(X_test, y_pred, title="Logistic Regression - Stochastic Gradient Descent", accuracy=acc)

       # Plot loss function value with respect to the number of iterations 
       plt.grid(linestyle='--' )
       plt.plot(clf.losses)
       plt.suptitle(' Stochastic Gradient Descent Optimizer', size = 18)
       plt.xlabel('Number of iterations', size=15)
       plt.ylabel('Binary Cross-Entropy Loss', size=15)
       plt.show()

if __name__ == "__main__":
    main()
