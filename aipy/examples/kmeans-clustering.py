import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

from aipy.utils import train_test_split, normalize, get_accuracy
from aipy.utils import euclidean_norm, Plot
from aipy.unsupervised import KMeans

def main():
    # Load the dataset
    X, y = datasets.make_blobs()

    # Cluster the data using K-Means
    clf = KMeans(k=3)
    y_pred = clf.predict(X)

    # Project the data onto the 2 primary principal components
    p = Plot()
    p.plot_in_2d(X, y_pred, title="K-Means Clustering")
    p.plot_in_2d(X, y, title="Actual Clustering")



if __name__ == "__main__":
    main()
