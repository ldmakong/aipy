import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

from aipy.utils import train_test_split, normalize, get_mse, get_mape
from aipy.utils import euclidean_norm, Plot
from aipy.supervised import NearestNeighborRegressor


boston = datasets.load_boston()
X = boston.data
y = boston.target

X = normalize(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)


# find hyperparameters that work best on the validation set
validation_error = []
for k in range(3, 4): #range(2, len(boston.data))  
  clf = NearestNeighborRegressor(k = k)
  clf.fit(X_train, y_train)
  y_pred = clf.predict(X_test)
  mse = get_mse(y_test, y_pred) 
  #print ('Mean squared error: %f' % (mse,))

  # keep track of what works on the validation set
  validation_error.append(mse)

# Color map
cmap = plt.get_cmap('viridis')

# Plot the results
m = plt.scatter(y_test, y_pred, color=cmap(0.5), s=10)
plt.suptitle("K Nearest Neighbors Regressor")
plt.title("MSE: %.2f" % mse, fontsize=10)
plt.xlabel('True')
plt.ylabel('Predicted')
#plt.legend((m1, m2), ("Training data", "Test data"), loc='lower right')
plt.show()

'''
plt.plot(range(2,len(boston.data)), validation_accuracies)
plt.xlabel('Number of neighbours')
plt.ylabel('Accuracy')
plt.grid()
plt.show()
'''
