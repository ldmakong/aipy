import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

from aipy.utils import train_test_split, normalize, get_accuracy
from aipy.utils import euclidean_norm, Plot
from aipy.supervised import NearestNeighborClassifier

iris = datasets.load_iris()
X = iris.data
y = iris.target

X = normalize(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)


# find hyperparameters that work best on the validation set
validation_accuracies = []
for k in range(13, 14): #range(2, len(iris.data))  
  # use a particular value of k and evaluation on validation data
  clf = NearestNeighborClassifier(k = k)
  clf.fit(X_train, y_train)
  y_pred = clf.predict(X_test)
  acc = get_accuracy(y_test, y_pred) 
  #print 'accuracy: %f' % (acc,)

  # keep track of what works on the validation set
  validation_accuracies.append(acc)

  Plot().plot_in_2d(X_test, y_pred, title="K Nearest Neighbors Classifier", accuracy=acc, legend_labels=iris.target_names)

'''
for k in range(2, len(iris.data)):
    clf = simple_knn(k)#Knnt(k)
    clf.fit(X, y)
    iris_pred = []
    for x in X:
        pred = clf.predict(x)
        iris_pred.append(pred)
    iris_target_pred = np.array(iris_pred)
    knn_iris_acc.append(accuracy(iris_target_pred, iris.target))
'''
'''
plt.plot(range(2,len(iris.data)), validation_accuracies)
plt.xlabel('Number of neighbours')
plt.ylabel('Accuracy')
plt.grid()
plt.show()
'''
