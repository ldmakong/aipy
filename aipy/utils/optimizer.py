import numpy as np

class GradientDescent():
    def __init__(self, w, grads, learning_rate=0.1):
        self.w = w
        self.dw = grads
        self.lr = learning_rate

    def update_weights(self):
        return self.w - self.lr * self.dw






