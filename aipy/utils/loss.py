import numpy as np
from aipy.utils import get_accuracy, Sigmoid


class binary_cross_entropy():
    """Computes the cross-entropy loss and gradient for binary classification.
    Applies the cross-entropy loss function to sigmoid activation for X and
    its weights.
    """
    def __init__(self, X, y, w, l1_beta=0.0, l2_beta=0.0):
        self.X = X
        self.y = y
        self.w = w
        self.l1_beta = l1_beta
        self.l2_beta = l2_beta
        self.sigmoid = Sigmoid()
  
    def value(self):
        
        # Clipped the predicted logistic probability p to value close to zero 
        # because of possible overflow from the exponent
        eps = np.finfo(np.float32).eps
        p =  np.clip(self.sigmoid(self.X.dot(self.w)), eps, 1.0-eps) 

        # Evaluate the loss 
        l = -np.mean(np.multiply(self.y, np.log(p)) + np.multiply((1-self.y), np.log(1-p)))

        #Add the l1 regularization 
        l += np.sqrt(np.dot(self.w.T, self.w)) * self.l1_beta 

        #Add the l2 regularization 
        l += np.dot(self.w.T, self.w) * self.l2_beta / 2

        # Evaluate the gradient
        grad = ((p - self.y).dot(self.X)/(self.X.shape[0])) + (self.l1_beta*(self.w/(np.abs(self.w) + 1e-8)) + self.l2_beta*self.w)

        return l, grad


class hinge():
    """Computes the hinge loss and gradient for SVM binary classification.
    """
    def __init__(self, X, y, w, l1_beta=0.0, l2_beta=0.0):
        self.X = X
        self.y = y
        self.w = w
        self.l1_beta = l1_beta
        self.l2_beta = l2_beta
  
    def value(self):
        
        margin = self.y*self.X.dot(self.w)

        # Evaluate the loss 
        l = np.sum(np.maximum(np.zeros_like(margin), 1 - margin)) / (self.X.shape[0])

        #Add the l1 regularization 
        l += np.sqrt(np.dot(self.w.T, self.w)) * self.l1_beta 

        #Add the l2 regularization 
        l += np.dot(self.w.T, self.w) * self.l2_beta / 2

        # Evaluate the gradient        
        yx = self.y.T*self.X
        grad = np.zeros_like(self.w)
        margin_selector = (margin < 1).ravel()
        for j in range(X.shape[1]):
            grad[j, 0] = (np.sum(np.where(margin_selector, -yx[:, j], 0)) /(self.X.shape[0])) 
        grad += (self.l1_beta*(self.w/(np.abs(self.w) + 1e-8)) + self.l2_beta*self.w)

        return l, grad
