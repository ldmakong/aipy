import numpy as np
import math
from aipy.utils import euclidean_distance


class KMeans():
    """A simple clustering method that forms k clusters by iteratively reassigning
    samples to the closest centroids and after that moves the centroids to the center
    of the new formed clusters.
    Parameters:
    -----------
    k: int
        The number of clusters the algorithm will form.
    max_iterations: int
        The number of iterations the algorithm will run for if it does
        not converge before that. 
    """
    def __init__(self, k=2, max_iterations=500):
        self.k = k
        self.max_iterations = max_iterations

    def create_clusters(self, X, centroids):
        """ Assign data to the closest centroids to create clusters """
        clusters = [[] for _ in range(self.k)]
        for label, data in enumerate(X):
            centroid_i = 0
            centroid_dist = float('inf')
            for i, centroid in enumerate(centroids):
                distance = euclidean_distance(data, centroid)
                if distance < centroid_dist:
                   centroid_i = i
                   centroid_dist = distance
            clusters[centroid_i].append(label)
        return clusters

    def calculate_centroids(self, clusters, X):
        """ Calculate new centroids as the means of the samples in each cluster  """
        centroids = np.zeros((self.k, np.shape(X)[1]))
        for i, cluster in enumerate(clusters):
            centroid = np.mean(X[cluster], axis=0)
            centroids[i] = centroid
        return centroids

    def get_cluster_labels(self, clusters, X):
        """ Classify samples as the index of their clusters """
        # One prediction for each sample
        y_pred = np.zeros(np.shape(X)[0])
        for cluster_i, cluster in enumerate(clusters):
            for sample_i in cluster:
                y_pred[sample_i] = cluster_i
        return y_pred

    def predict(self, X):
        """ K-Means clustering and return cluster indices """

        # Initialize centroids 
        centroids_idx = np.random.choice(range(X.shape[0]), self.k)
        centroids = X[centroids_idx]


        # Iterate until convergence or for max iterations
        for _ in range(self.max_iterations):
            # Assign data to closest centroids
            clusters = self.create_clusters(X, centroids)
            # Save current centroids for convergence check
            prev_centroids = centroids
            # Calculate new centroids from the clusters
            centroids = self.calculate_centroids(clusters, X)
            # If no centroids have changed => convergence
            diff = centroids - prev_centroids
            if not diff.any():
                break

        return self.get_cluster_labels(clusters, X)

