import numpy as np
import math
from aipy.utils import hinge
from aipy.utils import GradientDescent


class SupportVectorMachine():
    def __init__(self, max_iter = 4000, learning_rate=.1, optimizer='gd', l1_beta=0.0, l2_beta=0.0):
        self.max_iter = max_iter
        self.lr = learning_rate
        self.opt = optimizer
        self.l1_beta = l1_beta
        self.l2_beta = l2_beta
        self.weights= None
        self.losses= None

    def fit(self, X, y):
        # weights initialization
        weights = np.zeros(X.shape[1])
        
        # losses history initialization
        self.losses= []

        # Find and update weights to minimize the loss function
        for epoch in range(self.max_iter):

            # Setting up a vanilla gradient descent solver for the optimization
            if self.opt == 'gd': 
               # Set up the loss 
               lossfunc = hinge(X, y, weights, l1_beta=self.l1_beta, l2_beta=self.l2_beta)
               loss, gradient = lossfunc.value()
               # Set up the optimizer for the loss minimization 
               opt = GradientDescent(weights, gradient, learning_rate=self.lr)
               # Update weights with gradient
               weights = opt.update_weights()
               if epoch % 200 == 0:
                  print('epoch :', epoch, '---- loss :', loss)
               # Save the loss 
               self.losses.append(loss)
        self.weights = weights
        


    def predict(self, X):
        y_pred = np.argmax(X.dot(self.weights), axis =1)
        return y_pred
