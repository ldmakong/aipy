import numpy as np
import math
from aipy.utils import iterate_minibatches, Sigmoid
from aipy.utils import binary_cross_entropy
from aipy.utils import GradientDescent


class LogisticRegression():
    def __init__(self, max_iter = 4000, learning_rate=.1, optimizer='gd', batch_size = 1, shuffle=False, l1_beta=0.0, l2_beta=0.0):
        self.max_iter = max_iter
        self.lr = learning_rate
        self.opt = optimizer
        self.batch_size = batch_size
        self.shuffle=shuffle
        self.sigmoid = Sigmoid()
        self.l1_beta = l1_beta
        self.l2_beta = l2_beta
        self.weights= None
        self.losses= None

    def fit(self, X, y):
        # weights initialization
        weights = np.zeros(X.shape[1])
        
        # losses history initialization
        self.losses= []

        # Find and update weights to minimize the loss function
        for epoch in range(self.max_iter):

            # Setting up a vanilla gradient descent solver for the optimization
            if self.opt == 'gd':
               # Set up the loss 
               lossfunc = binary_cross_entropy(X, y, weights, l1_beta=self.l1_beta, l2_beta=self.l2_beta)
               loss, gradient = lossfunc.value()
               # Set up the optimizer for the loss minimization 
               opt = GradientDescent(weights, gradient, learning_rate=self.lr)
               # Update weights with gradient
               weights = opt.update_weights()
               if epoch % 200 == 0:
                  print('epoch :', epoch, '---- loss :', loss)
               # Save the loss 
               self.losses.append(loss)

            # Setting up a mini-batch stochastic gradient descent solver for the optimization
            if self.opt == 'sgd':

               epochLoss = []
               for X_batch, y_batch in iterate_minibatches(X, y, self.batch_size, shuffle=self.shuffle): 
                   # Set up the loss 
                   lossfunc = binary_cross_entropy(X_batch, y_batch, weights, l1_beta=self.l1_beta, l2_beta=self.l2_beta)
                   loss, gradient = lossfunc.value()
                   # Set up the optimizer for the loss minimization 
                   opt = GradientDescent(weights, gradient, learning_rate=self.lr)
                   # Update weights with gradient
                   weights = opt.update_weights()
                   epochLoss.append(loss)

               if epoch % 200 == 0:
                  print('epoch :', epoch, '---- loss :', np.average(epochLoss))
               # Save the average loss across all the batches
               self.losses.append(np.average(epochLoss))

        self.weights = weights
        


    def predict(self, X):
        y_pred = np.round(self.sigmoid(X.dot(self.weights))).astype(int)
        return y_pred
