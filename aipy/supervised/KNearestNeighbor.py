import numpy as np
import math
from aipy.utils import euclidean_norm


class NearestNeighborClassifier(object):
  def __init__(self, k, weights=None):
    self.k = k
    self.weights = weights

  def fit(self, X, y):
    """
    Train the classifier. For k-nearest neighbors this is just 
    memorizing the training data.
    Inputs:
    - X: A numpy array of shape (num_train, D) containing the training data
      consisting of num_train samples each of dimension D.
    - y: A numpy array of shape (N,) containing the training labels, where
         y[i] is the label for X[i].
    """
    self.X_train = X
    self.y_train = y


  def predict(self, X_test):
    """
    Predict labels for test data using this classifier.
    Inputs:
    - X: A numpy array of shape (num_test, D) containing test data consisting
         of num_test samples each of dimension D.
    - k: The number of nearest neighbors that vote for the predicted labels.
    - num_loops: Determines which implementation to use to compute distances
      between training points and testing points.
    Returns:
    - y: A numpy array of shape (num_test,) containing predicted labels for the
      test data, where y[i] is the predicted label for the test point X[i].  
    """
    dists = euclidean_norm(X_test, self.X_train)
    y_pred = np.zeros(dists.shape[0])
    for i in range(dists.shape[0]):
      neighbors = []        
      neighbors_idxs = np.argsort(dists[i, :])[:self.k]
      neighbors = self.y_train[neighbors_idxs]      
      y_pred[i] = np.argmax(np.bincount(neighbors))

    return y_pred  


class NearestNeighborRegressor(object):
  def __init__(self, k, weights=None):
    self.k = k
    self.weights = weights

  def fit(self, X, y):
    """
    Train the classifier. For k-nearest neighbors this is just 
    memorizing the training data.
    Inputs:
    - X: A numpy array of shape (num_train, D) containing the training data
      consisting of num_train samples each of dimension D.
    - y: A numpy array of shape (N,) containing the training labels, where
         y[i] is the label for X[i].
    """
    self.X_train = X
    self.y_train = y


  def predict(self, X_test):
    """
    Predict labels for test data using this classifier.
    Inputs:
    - X: A numpy array of shape (num_test, D) containing test data consisting
         of num_test samples each of dimension D.
    - k: The number of nearest neighbors that vote for the predicted labels.
    - num_loops: Determines which implementation to use to compute distances
      between training points and testing points.
    Returns:
    - y: A numpy array of shape (num_test,) containing predicted labels for the
      test data, where y[i] is the predicted label for the test point X[i].  
    """
    dists = euclidean_norm(X_test, self.X_train)
    y_pred = np.zeros(dists.shape[0])
    for i in range(dists.shape[0]):
      neighbors = []        
      neighbors_idxs = np.argsort(dists[i, :])[:self.k]
      neighbors = self.y_train[neighbors_idxs]      
      y_pred[i] = np.mean(neighbors)

    return y_pred
