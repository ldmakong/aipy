# AIPY

## Goals

Implementations from scratch of artificial intelligence models in Python. This ongoing project serves me as mental notes for current and future work in machine/deep learning. It also helps me better understand those models in order to debug and customize them for specific applications in the future.

## Requirements

- Python >=3.0
- [Anaconda](https://www.anaconda.com/)

First create a custom-named environment:

    $ conda env create -f environment.yml -n ENV_NAME

Second, update the environment to ensure all the necessary packages for the project are installed :

    $ conda env update -f environment.yml -n ENV_NAME

Finally, activate the environment :

    $ source activate ENV_NAME


## Models and results on toy datasets 
- Machine learning
  * [x] [K Nearest Neighbor](#k-nearest-neighbor) 
        + [x] [K Nearest Neighbor regression](#k-nearest-neighbor-regression)
        + [x] [K Nearest Neighbor classification](#k-nearest-neighbor-classification) 
  * [x] [K Means](#k-means) 
  * [x] [Logistic regression](#logistic-regression)
        + [x] [Gradient Descent on binary cross entropy loss](#gradient-descent-on-binary cross-entropy-loss) 
        + [x] [Stochastic Gradient Descent on binary cross entropy loss](#stochastic-gradient-descent-on-binary-cross-entropy-loss)

  * [ ] Support Vector Machine (in progress)
        + [ ] Gradient Descent on hinge loss
        + [ ] Stochastic Gradient Descent on hinge loss

- Deep learning (in progress - ANN, RNN, LSTM, RBM, Auto-encoders, etc ..)
- Reinforcement learning (in progress - Q-Learning, DQN, A2C, A3C, etc ..)

### K-Nearest Neighbor
#### K-Nearest Neighbor regression
    $ python -m aipy.examples.knn-regression

<p align="center">
    <img src="aipy/examples/assets/knn-reg.png" width="450">
</p>

#### K-Nearest Neighbor classification
    $ python -m aipy.examples.knn-classification

<p align="center">
    <img src="aipy/examples/assets/knn-class.png" width="450">
</p>

### K-Means
    $ python -m aipy.examples.kmeans-clustering

<p float="left">
  <img src="aipy/examples/assets/kmeans-cluster.png"  height="100%" width="400" />
  <img src="aipy/examples/assets/actual-cluster.png"  height="100%" width="400" /> 
</p>

### Logistic regression
#### Gradient descent on binary cross-entropy loss
    $ python -m aipy.examples.logistic-regression-gd

<p float="left">
  <img src="aipy/examples/assets/logistic-gd-class.png" width="400" />
  <img src="aipy/examples/assets/logistic-gd-loss.png" width="400" /> 
</p>

#### Stochastic Gradient Descent on binary cross-entropy loss
    $ python -m aipy.examples.logistic-regression-sgd

<p float="left">
  <img src="aipy/examples/assets/logistic-sgd-class.png" width="400" />
  <img src="aipy/examples/assets/logistic-sgd-loss.png" width="400" /> 
</p>



## References

- Work heavily inspired from [@eriklindernoren](https://github.com/eriklindernoren/ML-From-Scratch)
- [Standford CS231n](http://cs231n.github.io/)
- [Andrew Ng's coursera course](https://www.coursera.org/learn/machine-learning)
- [Overview of Gradient Descent algorithms](http://ruder.io/optimizing-gradient-descent/index.html#adam)

## Author

MAKONG Ludovic / [@ldmakong](https://gitlab.com/ldmakong)
